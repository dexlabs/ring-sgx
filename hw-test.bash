#!/bin/bash
#
set -e

source /opt/sgxsdk/environment
sleep 5s

cd /src/sgx/ring-sgx-test
make

cd ./bin
./app
